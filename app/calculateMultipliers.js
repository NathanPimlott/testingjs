exports.calculateCarAge = function(carReg)
{
  if(carReg.charAt(2) == "1")
  {
    return 1.2;
  }
  return 1.5;
}

exports.calculatePersonAgeMultiplier = function(age)
{
  return age < 25 ? 2: 1.2;
}

exports.calculateInsuranceGroupMultiplier = function(insuranceGroup)
{
  return insuranceGroup < 30 ? 1.2: 1.8;
}

exports.calculateNcdMultiplier = function(ncd)
{
  if(ncd < 2)
    return 2.5;
  else if (ncd < 5)
    return 1.8;
  return 1.2;
}

exports.calculateCityMultiplier = function(city)
{
  if (city == "sheffield")
  {
    return 1.8;
  }
  if (city == "london")
  {
    return 1.5;
  }
  else if (city == "manchester")
  {
    return 1.2;
  }
  return 1.0;
}

exports.calculateClaimsMultiplier = function(claims)
{
  return claims == 0 ? 1.0 : 2.0;
}

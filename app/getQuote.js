var Multipliers = require('./calculateMultipliers');

generatePolicy = function(carReg, personAge, insuranceGroup, ncd, city, claims)
{
  var policy = {
    carReg: carReg,
    personAge: personAge,
    insuranceGroup: insuranceGroup,
    ncd: ncd,
    city: city,
    claims: claims
  };
  return policy
}

exports.createNewPolicy = function(policy)
{
  var basePremium = 200;
  basePremium *= Multipliers.calculateCarAge(policy.carReg);
  basePremium *= Multipliers.calculatePersonAgeMultiplier(policy.PersonAge);
  basePremium *= Multipliers.calculateInsuranceGroupMultiplier(policy.InsuranceGroup);
  basePremium *= Multipliers.calculateNcdMultiplier(policy.Ncd);
  basePremium *= Multipliers.calculateCityMultiplier(policy.City);
  basePremium *= Multipliers.calculateClaimsMultiplier(policy.Claims);

  console.log("The premium for your policy is: " + basePremium);
  return basePremium;
}

var expect = require("chai").expect;
var getQuote = require("../app/getQuote");
var Multipliers = require("../app/calculateMultipliers");

var newDriver = new generatePolicy("MN03YHN", 17, 15, 0, "sheffield", 0);
var oldDriver = new generatePolicy("MN15YHN", 45, 35, 15, "london", 1);
getQuote.createNewPolicy(newDriver);
getQuote.createNewPolicy(oldDriver);

describe("Get Full Policies", function() {

  //GET FULL POLICIES

  describe("Young Driver", function() {
    it("When I am newDriver the premium should be", function() {
      var policy = getQuote.createNewPolicy(newDriver).toFixed(2);
      expect(policy).to.equal('1555.20');
    });
  });

  describe("Old Driver", function() {
    it("When I am oldDriver the premium should be", function() {
      var policy = getQuote.createNewPolicy(oldDriver).toFixed(2);
      console.log(policy.basePremium);
      expect(policy).to.equal('1244.16');
    });
  });
});

describe("Get Multipliers For Policies", function() {

  //CAR REG TESTS

  describe("Car Reg Tests", function() {
    it("When a car reg is newer than 2010 the premium modifier should be 1.2", function() {
      var modifier = Multipliers.calculateCarAge("PY11YHN");
      expect(modifier).to.equal(1.2);
    });
  });

  describe("Car Reg Tests", function() {
    it("When a car reg is older than 2010 the premium modifier should be 1.5", function() {
      var modifier = Multipliers.calculateCarAge("PY04YHN");
      expect(modifier).to.equal(1.5);
    });
  });


  //PERSON AGE TESTS

  describe("Person age tests", function() {
    it("When the person is older than 24 the multiplier should be 1.2", function() {
      var modifier = Multipliers.calculatePersonAgeMultiplier(35);
      expect(modifier).to.equal(1.2);
    });
  });

  describe("Person age tests", function() {
    it("When the person is younger than 24 the multiplier should be 2", function() {
      var modifier = Multipliers.calculatePersonAgeMultiplier(18);
      expect(modifier).to.equal(2);
    });
  });


  //INSURANCE GROUP TESTS

  describe("insurance group tests", function() {
    it("When the insurance group is less than 30 the multiplier should be 1.2", function() {
      var modifier = Multipliers.calculateInsuranceGroupMultiplier(20);
      expect(modifier).to.equal(1.2);
    });
  });

  describe("insurance group tests", function() {
    it("When the insurance group is greater than 30 the multiplier should be 1.8", function() {
      var modifier = Multipliers.calculateInsuranceGroupMultiplier(35);
      expect(modifier).to.equal(1.8);
    });
  });


  //NCD TESTS

  describe("ncd tests", function() {
    it("When the ncd is less than 2 the multiplier should be 2.5", function() {
      var modifier = Multipliers.calculateNcdMultiplier(1);
      expect(modifier).to.equal(2.5);
    });
  });

  describe("ncd tests", function() {
    it("When the ncd is less than 5 but greater than 2 the multiplier should be 1.8", function() {
      var modifier = Multipliers.calculateNcdMultiplier(4);
      expect(modifier).to.equal(1.8);
    });
  });

  describe("ncd tests", function() {
    it("When the ncd is greater than 5 the multiplier should be 1.2", function() {
      var modifier = Multipliers.calculateNcdMultiplier(12);
      expect(modifier).to.equal(1.2);
    });
  });


  //LOCATION TESTS

  describe("Location Tests", function() {
    it("When the location is sheffield then the multiplier should be 1.8", function() {
      var modifier = Multipliers.calculateCityMultiplier("sheffield");
      expect(modifier).to.equal(1.8);
    });
  });

  describe("Location Tests", function() {
    it("When the location is london then the multiplier should be 1.5", function() {
      var modifier = Multipliers.calculateCityMultiplier("london");
      expect(modifier).to.equal(1.5);
    });
  });

  describe("Location Tests", function() {
    it("When the location is manchester then the multiplier should be 1.2", function() {
      var modifier = Multipliers.calculateCityMultiplier("manchester");
      expect(modifier).to.equal(1.2);
    });
  });

  describe("Location Tests", function() {
    it("When the location is anything other than manchester, sheffield or london then the multiplier should be 1.0", function() {
      var modifier = Multipliers.calculateCityMultiplier("blackpool");
      expect(modifier).to.equal(1.0);
    });
  });


  //CLAIMS TESTS

  describe("Claims Tests", function() {
    it("When the customer has had no claims then the multiplier should be 1", function() {
      var modifier = Multipliers.calculateClaimsMultiplier(0);
      expect(modifier).to.equal(1.0);
    });
  });

  describe("Claims Tests", function() {
    it("When the customer has had at least 1 claim then the multiplier should be 2", function() {
      var modifier = Multipliers.calculateClaimsMultiplier(2);
      expect(modifier).to.equal(2.0);
    });
  });
});
